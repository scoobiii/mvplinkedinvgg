MVPLinkedinVGG 🚀

O MVPLinkedinVGG é um projeto que fornece uma implementação de vários algoritmos de aprendizado de máquina para reconhecimento de imagens. Os algoritmos implementados são:

* Redes Neurais Convolucionais (CNNs)
* K-Vizinhos mais próximos (KNN)
* Máquinas de vetor de suporte (SVMs)

O projeto também inclui um script para organizar os conjuntos de dados e um script para treinar e avaliar os algoritmos de aprendizado de máquina.

## Como usar

Para usar o projeto, siga estas etapas:

1. Clone o repositório do projeto para o seu computador.
2. Abra um terminal e navegue até o diretório do repositório.
3. Instale as dependências do projeto usando o seguinte comando: